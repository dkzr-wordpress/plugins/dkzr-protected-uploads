jQuery( function($) {
  // update url in media browser after save
  $(document).ajaxComplete( function(e, xhr, settings) {
    if (
      settings.data
      && -1 !== settings.data.indexOf('action=save-attachment-compat')
      && xhr.responseJSON
      && xhr.responseJSON.data
      && xhr.responseJSON.data.id
      && xhr.responseJSON.data.url
    ) {
      $('.attachment-details[data-id="' + xhr.responseJSON.data.id + '"] [data-setting="url"] input').attr( 'value', xhr.responseJSON.data.url );
    }
  } );

  var setIcon = function( $el, protectedUpload ) {
    if( protectedUpload ) {
      $el.addClass('protected-upload');
      $el.append('<span class="protected-upload-icon dashicons dashicons-shield" tabindex="-1"></span>');
    } else {
      $el.removeClass('protected-upload');
      $el.find('.protected-upload-icon').remove();
    }
  };

  //var Attachment = wp.media.view.Attachment;
  //wp.media.view.Attachment = Attachment.extend( {
  //  render: function() {
  //    Attachment.prototype.render.apply( this, arguments );

  //    setIcon( this.$el, this.model.attributes.protectedUpload );

  //    return this;
  //  }
  //} );

  var AttachmentLibrary = wp.media.view.Attachment.Library;
  wp.media.view.Attachment.Library = AttachmentLibrary.extend( {
    render: function() {
      AttachmentLibrary.prototype.render.apply( this, arguments );

      setIcon( this.$el, this.model.attributes.protectedUpload );

      return this;
    }
  } );
} );
