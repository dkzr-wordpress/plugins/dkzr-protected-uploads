Protected Uploads
=================

WordPress plugin that adds access restrictions to specific items of the WordPress Media Library.
