<?php
/**
 * Plugin Name: Protected Uploads
 * Plugin URI: https://gitlab.com/dkzr-wordpress/plugins/dkzr-protected-uploads
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/protected-uploads
 * Text Domain: dkzr-protected-uploads
 * Description: Add access restrictions to specific items of the WordPress Media Library.
 * Version: 8.1.2
 * Author: Joost de Keijzer, based on work by Alexandre Froger
 * Requires at least: 6.0
 * Requires PHP: 8.0
 */

require_once ABSPATH . 'wp-admin/includes/file.php';

class dkzrProtectedUploads {
  private $dataDirName = 'protected-uploads';
  private $protectedDirName = 'protected';

  public function __construct() {
    add_action( 'admin_init', [$this, 'admin_init']);
    add_action( 'delete_attachment', [$this, 'delete_attachment'] );
    add_action( 'init', [$this, 'init'] );
    add_action( 'parse_request', [$this, 'parse_request'], -10 );

    add_filter( '_wp_relative_upload_path', [$this, '_wp_relative_upload_path'], -10, 2 );
    add_filter( 'add_post_metadata', [$this, 'add_post_metadata'], 10, 5 );
    add_filter( 'display_media_states', [$this, 'display_media_states'], 10, 2 );

    add_filter( 'get_attached_file', [$this, 'get_attached_file'], -10, 2 );
    add_filter( 'wp_get_attachment_url', [$this, 'wp_get_attachment_url'], -10, 2 );
    add_filter( 'wp_calculate_image_srcset', [$this, 'wp_calculate_image_srcset'], 10, 5 );
    add_filter( 'wp_prepare_attachment_for_js', [$this, 'wp_prepare_attachment_for_js'], 10, 3 );
  }

  public function get_protected_uploads_dirname() {
    _deprecated_function( __METHOD__, 'Protected Uploads 8.0' );
    return $this->dataDirName;
  }

  public function get_protected_dirname() {
    return $this->protectedDirName;
  }

/**
 * Get absolute path to private uploads basedir.
 *
 * Since v8 of this plugin this folder is a subdirectory of
 * `WP_CONTENT_DIR . '/uploads'`. For single sites it defaults to
 * `WP_CONTENT_DIR . '/uploads/protected/'`. For recent multisite subsites it
 * defaults to `WP_CONTENT_DIR . '/uploads/sites/{$blog_id}/protected/'`
 */
  public function get_basedir() {
    static $cache = [];

    $blog_id = get_current_blog_id();

    // we have cache?
    // return
    if ( ! empty( $cache[$blog_id] ) ) {
      return $cache[$blog_id];
    }

    $upload_dir = wp_get_upload_dir();
    $cache[$blog_id] = trailingslashit( trailingslashit( $upload_dir['basedir'] ) . $this->get_protected_dirname() );

    return $cache[$blog_id];
  }

  public function get_baseurl( $relative = false, $basedir = null ) {
    $url = str_replace( ABSPATH, '', $basedir ?? $this->get_basedir() );
    if ( $relative ) {
      return $url;
    } else {
      return home_url( $url );
    }
  }

  protected function pre_v8_protected( $attachment_id ) {
    $output = get_metadata_raw( 'post', $attachment_id, 'dkzr_protected_uploads_is_private', true );
    if ( is_null( $output ) || false === $output ) {
      return NULL;
    } else {
      return !empty( $output );
    }
  }

/**
 * Get absolute path to *pre v8* private uploads basedir.
 *
 * This is always a folder next to the "uploads" dir defined in WordPress,
 * so it defaults to WP_CONTENT_DIR . '/protected-uploads/'.
 */
  protected function get_pre_v8_basedir() {
    static $cache = [];

    $blog_id = get_current_blog_id();

    // we have cache?
    // return
    if ( ! empty( $cache[$blog_id] ) ) {
      return $cache[$blog_id];
    }

    $upload_dir = wp_get_upload_dir();

    // @see _wp_upload_dir()
    if ( is_multisite() && ! ( is_main_network() && is_main_site() && defined( 'MULTISITE' ) ) ) {
      // multisite installs always put their files in "wp-content/", but depending
      // on the version in "wp-content/uploads/sites/" or "wp-content/blogs.dir/"
      $basedir = trailingslashit( $upload_dir['basedir'] );

      if ( ! get_site_option( 'ms_files_rewriting' ) ) {
          $basedir = str_replace( WP_CONTENT_DIR . '/uploads', WP_CONTENT_DIR . '/' . $this->dataDirName, $basedir );
      } elseif ( defined( 'UPLOADS' ) && ! ms_is_switched() ) {
        if ( defined( 'BLOGUPLOADDIR' ) ) {
          $dir = WP_CONTENT_DIR;
        } else {
          $dir = dirname( ABSPATH . UPLOADBLOGSDIR );
        }
        $basedir = str_replace( $dir, ABSPATH . 'wp-content/' . $this->dataDirName, $basedir );
      }
    } else {
      $basedir = trailingslashit( trailingslashit( dirname( $upload_dir['basedir'] ) ) . $this->dataDirName );
    }

    $cache[$blog_id] = apply_filters_deprecated( 'dkzr_protected_uploads_basedir', [ $basedir, $blog_id ], 'Protected Uploads 8.0' );

    return $cache[$blog_id];
  }

  protected function get_rules() {
    $rules = ['_deny_all' => true, '_require_login' => true ];
    foreach( apply_filters( 'dkzr_protected_uploads_roles', $GLOBALS['wp_roles']->get_names() ) as $role => $name ) {
      $rules[ $role ] = sprintf( __( 'Require "%s" role', 'dkzr-protected-uploads' ), $name );
    }

    return apply_filters( 'dkzr_protected_uploads_rules', $rules );
  }

/**
 * Based on URL, returns file_url, file and attachment_id.
 */
  protected function url_to_protected_file( $url ) {
    $basedirs = get_option( 'dkzr_protected_uploads_previous_basedirs', [] );
    $basedirs[] = $this->get_basedir();

    $file_url = $file = $attachment_id = null;

    // loop through all (legacy) protected-uploads basedirs, newest first.
    foreach( array_reverse( $basedirs ) as $basedir ) {
      $baseurl = $this->get_baseurl( true, $basedir );

      // for v8+ remove 'protected' part from dir & url
      if ( $basedir == $this->get_basedir() ) {
        $basedir = trailingslashit( dirname( $basedir ) );
        $baseurl = trailingslashit( dirname( $baseurl ) );
      }

      if ( 0 === strpos( $url, $baseurl ) ) {
        $file_url = substr( $url, strlen( $baseurl ) );
      }

      if ( empty( $file_url ) ) {
        continue;
      }

      $file = $basedir . $file_url;
      // "protected-uploads" url but file doesn't exist pt. 1
      // die;
      if ( ! is_file( $file ) ) {
        wp_die( new WP_Error( 'http_404', __( 'File not found', 'dkzr-protected-uploads' ), ['status' => 404] ) );
        exit();
      }

      $attachment_id = $this->file_url_to_attachment_id( $file_url );
      // "protected-uploads" url but file doesn't exist pt. 2
      // die;
      if ( ! $attachment_id ) {
        wp_die( new WP_Error( 'http_404', __( 'File not found', 'dkzr-protected-uploads' ), ['status' => 404] ) );
        exit();
      } else {
        // We found the file!
        break;
      }
    }

    return compact( 'file_url', 'file', 'attachment_id' );
  }

  protected function file_url_to_attachment_id( $file_url ) {
    global $wpdb;

    // find in '_wp_attached_file'
    $query = $wpdb->prepare( "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key = '_wp_attached_file' AND meta_value = %s;", $file_url );
    $attachment_id = $wpdb->get_var( $query );

    if ( ! $attachment_id ) {
      // find in '_wp_attachment_metadata'
      $file_url_basename = basename( $file_url );
      $query = $wpdb->prepare( "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key = '_wp_attachment_metadata' AND meta_value LIKE %s;", '%' . $wpdb->esc_like( $file_url_basename ) . '%' );
      $attachment_ids = $wpdb->get_col( $query );

      // check directory for found attachments
      if ( $attachment_ids ) {
        foreach ( $attachment_ids as $post_id ) {
          $attached_file = pathinfo( (string) get_post_meta( $post_id, '_wp_attached_file', true ) );
          if (
            $attached_file
            && isset( $attached_file['dirname'], $attached_file['filename'] )
            && str_starts_with( $file_url, "{$attached_file['dirname']}/{$attached_file['filename']}" )
          ) {
            $attachment_id = $post_id;
            break;
          }
        }
      }
    }

    return (int) $attachment_id;
  }

  protected function init_filesystem() {
    global $wp_filesystem;
    if ( ! is_object( $wp_filesystem ) ) {
      WP_Filesystem();
    }
  }

  protected function maybe_setup_directories() {
    $this->init_filesystem();

    global $wp_filesystem, $wp_rewrite;

    $home_root = parse_url( home_url() );
    if ( is_multisite() ) {
      $home_root = parse_url( network_home_url() );
    }

    if ( isset( $home_root['path'] ) ) {
      $home_root = trailingslashit( $home_root['path'] );
    } else {
      $home_root = '/';
    }

    $basedir = $this->get_basedir();

    wp_mkdir_p( $basedir );

    if ( ! $wp_filesystem->is_file( $basedir . '.htaccess' ) ) {
      $rules  = "# DO NOT EDIT #\n";
      $rules .= "# All requests are send to WordPress which will either deny access, allow access\n";
      $rules .= "# or respond with 404 Not Found (when the plugin is disabled).\n";
      $rules .= "<IfModule mod_rewrite.c>\n";
      $rules .= "RewriteEngine On\n";
      $rules .= "RewriteBase $home_root\n";
      $rules .= "RewriteRule . {$home_root}{$wp_rewrite->index} [L]\n";
      $rules .= "</IfModule>\n";

      insert_with_markers( $basedir . '.htaccess', 'dkzr Protected Uploads', $rules );
    }

    if ( ! $wp_filesystem->is_file( $basedir . 'web.config' ) ) {
      $rules = [];
      $rules[] = '<?xml version="1.0" encoding="UTF-8"?>';
      $rules[] = '<!--';
      $rules[] = '  - DO NOT EDIT -';
      $rules[] = '  - All requests are send to WordPress which will either deny access, allow access';
      $rules[] = '  - or respond with 404 Not Found (when the plugin is disabled).';
      $rules[] = '  -->';
      $rules[] = '<configuration>';
      $rules[] = '  <system.webServer>';
      $rules[] = '    <directoryBrowse enabled="false" />';
      $rules[] = '    <rewrite>';
      $rules[] = '      <rules>';
      $rules[] = '        <rule name="dkzr Protected Uploads" patternSyntax="Wildcard" stopProcessing="true">';
      $rules[] = '          <match url="*" />';
      $rules[] = '          <action type="Rewrite" url="' . $home_root . 'index.php" />';
      $rules[] = '        </rule>';
      $rules[] = '      </rules>';
      $rules[] = '    </rewrite>';
      $rules[] = '  </system.webServer>';
      $rules[] = '</configuration>';
      file_put_contents( $basedir . 'web.config', implode( "\n", $rules ) );
    }
  }

  public function get_permissions( $attachment_id ) {
    $permissions = get_post_meta( $attachment_id, 'dkzr_protected_uploads_permissions', true );

    // should not be empty...
    if ( empty( $permissions ) || ! is_array( $permissions ) ) {
      $permissions = [];
    }

    return $permissions;
  }

  public function is_allowed( $attachment_id, $allow = false, $file_url = '' ) {
    $permissions = $this->get_permissions( $attachment_id );

    if ( empty( $permissions ) ) {
      return $allow;
    }

    if ( empty( $file_url ) ) {
      $file_url = get_post_meta( $attachment_id, '_wp_attached_file', true );
    }

    $permissions = $this->get_permissions( $attachment_id );

    foreach( $permissions as $rule => $value ) {
      if ( $value && current_user_can( $rule ) ) {
        $allow = true;
      }
    }

    if ( is_user_logged_in() &&  isset( $permissions['_require_login'] ) && $permissions['_require_login'] ) {
      $allow = true;
    }

    if ( isset( $permissions['_deny_all'] ) && $permissions['_deny_all'] ) {
      $allow = false;
    }

    $allow = apply_filters( 'dkzr_protected_uploads_attachment_permission', $allow, $permissions, $attachment_id, $file_url );
    $allow = apply_filters( 'dkzr_protected_uploads_attachment_permission_on_read', $allow, $permissions, $attachment_id, $file_url );

    return $allow;
  }

/**
 * Actually move the file(s)
 * This is done *before* the permissions post-meta of the attachment is updated!
 */
// TODO: set quid for POST (so eg. enable-media-replace might work)
  protected function move_media( $attachment_id, $operation, $attachment_data = null ) {
    $current_permissions = $this->get_permissions( $attachment_id );

    if (
      ( 'public' === $operation && empty( $current_permissions ) )
      || ( 'public' !== $operation && !empty( $current_permissions ) )
    ) {
      // no change in state, files should be at the correct location
      return;
    }

    $this->init_filesystem();

    global $wp_filesystem;

    $upload_dir       = wp_get_upload_dir();

    $public_dir       = trailingslashit( $upload_dir['basedir'] );
    $private_dir      = $this->get_basedir();

    $destination_basedir = ( 'public' === $operation ) ? $public_dir : $private_dir;
    $source_basedir      = ( 'public' === $operation ) ? $private_dir : $public_dir;

    $pre_v8_private = $this->pre_v8_protected( $attachment_id );

    if ( 'public' === $operation && $pre_v8_private ) {
      $private_dir = $source_basedir = $this->get_pre_v8_basedir();

      $wp_file_path     = get_attached_file( $attachment_id, true ); // !!! unfiltered !!!
      $wp_file_basedir  = trailingslashit( pathinfo( $wp_file_path, PATHINFO_DIRNAME ) );
      $subdir           = trailingslashit( preg_replace( '#^' . preg_quote( $public_dir ) . '#', '', $wp_file_basedir, 1 ) );
      $files            = array( preg_replace( '#^' . preg_quote( $public_dir ) . '#', $source_basedir, $wp_file_path, 1 ) );
    } else {
      $wp_file_path     = get_attached_file( $attachment_id );
      $wp_file_basedir  = trailingslashit( pathinfo( $wp_file_path, PATHINFO_DIRNAME ) );
      $subdir           = trailingslashit( preg_replace( '#^' . preg_quote( $source_basedir ) . '#', '', $wp_file_basedir, 1 ) );
      $files            = array( $wp_file_path );
    }

    $filename_base    = pathinfo( $wp_file_path, PATHINFO_BASENAME );
    $filename_ext     = pathinfo( $wp_file_path, PATHINFO_EXTENSION );
    if ( $filename_ext ) {
        $filename_ext  = '.' . $filename_ext;
        $filename_base = substr( $filename_base, 0, - strlen( $filename_ext ) );
    }

    if ( empty( $attachment_data ) || empty( $attachment_data['sizes'] ) ) {
      $attachment_data = (array) $attachment_data;
      $current_data = wp_get_attachment_metadata( $attachment_id, true );
      if ( ! empty( $current_data['sizes'] ) ) {
        $attachment_data['sizes'] = $current_data['sizes'];
      }
    }

    if ( isset( $attachment_data['sizes'] ) && ! empty( $attachment_data['sizes'] ) ) {
      foreach ( $attachment_data['sizes'] as $size_info ) {
        $files[] = $source_basedir . $subdir . $size_info['file'];
      }
    }

    if ( ! empty( $subdir ) ) {
      $subdir_path_fragments = explode( '/', untrailingslashit( $subdir ) );
      $partial_path   = $destination_basedir;

      foreach ( $subdir_path_fragments as $fragment ) {
        if ( ! $wp_filesystem->is_dir( $partial_path . $fragment ) ) {
          $wp_filesystem->mkdir( $partial_path . $fragment );
        }

        $partial_path = trailingslashit( $partial_path . $fragment );
      }
    }

    $new_filename_base = wp_unique_filename( "{$destination_basedir}{$subdir}", pathinfo( $wp_file_path, PATHINFO_BASENAME ) );
    if ( $filename_ext ) {
        $new_filename_base = substr( $new_filename_base, 0, - strlen( $filename_ext ) );
    }

    foreach ( $files as $file ) {
      $source             = $file;
      $dest_dirname       = preg_replace( '#^' . preg_quote( $source_basedir ) . '#', $destination_basedir, trailingslashit( pathinfo( $file, PATHINFO_DIRNAME ) ), 1 );

      $dest_filename_base = pathinfo( $file, PATHINFO_BASENAME );
      $dest_filename_ext  = pathinfo( $file, PATHINFO_EXTENSION );
      if ( $dest_filename_ext ) {
        $dest_filename_ext  = '.' . $dest_filename_ext;
        $dest_filename_base = substr( $dest_filename_base, 0, - strlen( $dest_filename_ext ) );
      }

      $dest_filename_base = preg_replace( '#^' . preg_quote( $filename_base ) . '#', $new_filename_base, $dest_filename_base );

      if ( $wp_filesystem->is_file( $source ) ) {
        $wp_filesystem->move( $source, $dest_dirname . $dest_filename_base . $dest_filename_ext, true );
      }
    }

    if ( empty( $pre_v8_private ) || $filename_base != $new_filename_base ) {
      update_attached_file( $attachment_id, "{$destination_basedir}{$subdir}{$new_filename_base}{$filename_ext}" );

      $metadata = wp_get_attachment_metadata( $attachment_id, true );
      if ( isset( $metadata['file'] ) ) {
        if ( $wp_attached_file = get_post_meta( $attachment_id, '_wp_attached_file', true ) ) {
          $metadata['file'] = $wp_attached_file;
        } else {
          unset( $metadata['file'] );
        }
      }

      if ( isset( $attachment_data['sizes'] ) && ! empty( $attachment_data['sizes'] ) && $filename_base != $new_filename_base ) {
        foreach( array_keys( $attachment_data['sizes'] ) as $key ) {
          if ( isset( $metadata['sizes'][$key]['file'] ) ) {
            $metadata['sizes'][$key]['file'] = preg_replace( '#^' . preg_quote( $filename_base ) . '#', $new_filename_base, $metadata['sizes'][$key]['file'] );
          }
        }
      }

      wp_update_attachment_metadata( $attachment_id, $metadata );
    }

    if ( ! is_null( $pre_v8_private ) ) {
      delete_post_meta( $attachment_id, 'dkzr_protected_uploads_is_private' );
    }
  }

// =============================================================================
// mark: Actions
// =============================================================================

  public function admin_init() {
    add_action( 'wp_enqueue_media', [$this, 'wp_enqueue_media'] );

    add_filter( 'attachment_fields_to_edit', array( $this, 'attachment_fields_to_edit' ), 10, 2 );
    add_filter( 'attachment_fields_to_save', array( $this, 'attachment_fields_to_save' ), 10, 2 );

    $this->maybe_setup_directories();

    // Enable Media Replace
    if ( is_plugin_active( 'enable-media-replace/enable-media-replace.php' ) ) {
      $emrData = get_plugin_data( WP_PLUGIN_DIR . '/enable-media-replace/enable-media-replace.php' );
      if ( isset( $emrData['Version'] ) && 0 <= version_compare( $emrData['Version'], '3.3' ) ) {
        // Version 3.3+ is supported
        add_filter( 'emr_unfiltered_get_attached_file', '__return_false', 9999 );
        add_action( 'wp_handle_replace', [$this, 'emr_wp_handle_replace'], 10, 1 );
      } else {
        // Version < 3.3 is not supported
        add_action( 'attachment_fields_to_edit', [$this, 'emr_attachment_fields_to_edit'], 9999, 2 );
        add_action( 'emr_before_replace_type_options', [$this, 'emr_before_replace_type_options'] );
      }
    }
  }

/**
 * Delete attachment files early
 */
  public function delete_attachment( $post_id ) {
    $permissions = get_post_meta( $post_id, 'dkzr_protected_uploads_permissions', true );

    if ( ! empty( $permissions ) ) {
      $meta         = wp_get_attachment_metadata( $post_id );
      $backup_sizes = get_post_meta( $post_id, '_wp_attachment_backup_sizes', true );
      $file         = get_attached_file( $post_id );

      // delete all files
      wp_delete_attachment_files( $post_id, $meta, $backup_sizes, $file );

      // the file itself should fail, so retry with correct basedir
      wp_delete_file_from_directory( $file, $this->get_basedir() );
    }
  }

  public function init() {
    load_plugin_textdomain( 'dkzr-protected-uploads', false, basename( dirname( __FILE__ ) ) . '/languages' );

    $this->dataDirName = trim( apply_filters_deprecated( 'dkzr_protected_uploads_datadir_name', [ $this->dataDirName ], 'Protected Uploads 8.0' ), '/' );
    $this->protectedDirName = trim( apply_filters( 'dkzr_protected_uploads_dirname', $this->protectedDirName ), '/' );

    $basedir = $this->get_basedir();

    $current = get_option( 'dkzr_protected_uploads_current_basedir' );
    if ( $current && $current !== $basedir ) {
      $previous = get_option( 'dkzr_protected_uploads_previous_basedirs', [] );
      $previous[] = $current;
      update_option( 'dkzr_protected_uploads_previous_basedirs', array_unique( $previous ), 'no' );
    }
    update_option( 'dkzr_protected_uploads_current_basedir', $basedir, 'no' );

    /*** non-wp actions and filters ***/

    // Ai1wm support
    if ( is_multisite() ) {
      add_filter( 'ai1wm_export', [$this, 'ai1wmme_export_enumerate_media'], 115 );
    } else {
      add_filter( 'ai1wm_export', [$this, 'ai1wm_export_enumerate_media'], 115 );
    }

    add_filter( 'ai1wm_export', [$this, 'ai1wm_export_media'], 165 );
  }

  public function parse_request( $wp ) {
    $url = '';
    $file_url = false;

    // get requested url
    if ( isset( $wp->query_vars['pagename'] ) ) {
      $url = $wp->query_vars['pagename'];
    } else if ( isset( $wp->request ) && is_string( $wp->request ) ) {
      $url = $wp->request;
    }

    if ( str_ends_with( $url, '.htaccess' ) || str_ends_with( $url, 'web.config' ) ) {
      wp_die( new WP_Error( 'http_403', __( 'Forbidden', 'dkzr-protected-uploads' ), ['status' => 403] ) );
      exit();
    }

    // extracts file_url, file and attachment_id
    $file_url = $file = $attachment_id = null;
    extract( $this->url_to_protected_file( $url ) );

    // Not a "protected-uploads" url
    // return and continue WordPress
    if ( empty( $file_url ) ) {
      return;
    }

    // Double check
    if ( ! is_file( $file ) || ! $attachment_id ) {
      wp_die( new WP_Error( 'http_404', __( 'File not found', 'dkzr-protected-uploads' ), ['status' => 404] ) );
      exit();
    }

    $allow = $this->is_allowed( $attachment_id, false, $file_url );

    // "protected-uploads" url and file exists but user doesn't have access
    // die;
    // wp_die & exit;
    if ( ! $allow ) {
      if (is_user_logged_in() ) {
        wp_die( new WP_Error( 'http_403', __( "You don't have permission to view this protected file.", 'dkzr-protected-uploads' ), ['status' => 403] ) );
      } else {
        wp_die( new WP_Error( 'http_401', __( 'This is a protected file, please login and try again.', 'dkzr-protected-uploads' ), ['status' => 401] ) );
      }
      exit();
    }

    // all clear, offer file for download
    // code below taken from wp-includes/ms-files.php
    error_reporting( 0 );

    $mime = wp_check_filetype( $file, wp_get_mime_types() );
    if ( false === $mime['type'] && function_exists( 'mime_content_type' ) ) {
      $mime['type'] = mime_content_type( $file );
    }

    if ( $mime['type'] ) {
      $mimetype = $mime['type'];
    } else {
      $mimetype = 'image/' . substr( $file, strrpos( $file, '.' ) + 1 );
    }

    header( 'Content-Type: ' . $mimetype ); // always send this
    if ( false === strpos( $_SERVER['SERVER_SOFTWARE'], 'Microsoft-IIS' ) ) {
      header( 'Content-Length: ' . filesize( $file ) );
    }

    do_action( 'dkzr_protected_uploads_headers', $file_url, $attachment_id, $wp );

    if ( is_multisite() ) {
      ms_file_constants();
      // Optional support for X-Sendfile and X-Accel-Redirect
      if ( WPMU_ACCEL_REDIRECT ) {
        header( 'X-Accel-Redirect: ' . str_replace( WP_CONTENT_DIR, '', $file ) );
        exit;
      } elseif ( WPMU_SENDFILE ) {
        header( 'X-Sendfile: ' . $file );
        exit;
      }
    }

    $last_modified = gmdate( 'D, d M Y H:i:s', filemtime( $file ) );
    $etag          = '"' . md5( $last_modified ) . '"';
    header( "Last-Modified: $last_modified GMT" );
    header( 'ETag: ' . $etag );
    header( 'Expires: ' . gmdate( 'D, d M Y H:i:s', time() + 100000000 ) . ' GMT' );

    // Support for Conditional GET - use stripslashes to avoid formatting.php dependency
    $client_etag = isset( $_SERVER['HTTP_IF_NONE_MATCH'] ) ? stripslashes( $_SERVER['HTTP_IF_NONE_MATCH'] ) : false;

    if ( ! isset( $_SERVER['HTTP_IF_MODIFIED_SINCE'] ) ) {
      $_SERVER['HTTP_IF_MODIFIED_SINCE'] = false;
    }

    $client_last_modified = trim( $_SERVER['HTTP_IF_MODIFIED_SINCE'] );
    // If string is empty, return 0. If not, attempt to parse into a timestamp
    $client_modified_timestamp = $client_last_modified ? strtotime( $client_last_modified ) : 0;

    // Make a timestamp for our most recent modification...
    $modified_timestamp = strtotime( $last_modified );

    if ( ( $client_last_modified && $client_etag )
      ? ( ( $client_modified_timestamp >= $modified_timestamp ) && ( $client_etag == $etag ) )
      : ( ( $client_modified_timestamp >= $modified_timestamp ) || ( $client_etag == $etag ) )
      ) {
      status_header( 304 );
      exit();
    }

    // If we made it this far, just serve the file

    // disable all output buffering
    while ( @ob_end_flush() );

    readfile( $file );
    flush();
    exit();
  }

  public function wp_enqueue_media() {
    wp_enqueue_style( 'dkzr-protected-uploads-admin-css', plugins_url( 'css/protected-uploads-admin.css', __FILE__ ), [] );
    wp_enqueue_script( 'dkzr-protected-uploads-admin-js', plugins_url( 'js/protected-uploads-admin.js', __FILE__ ), ['media-views'] );
  }

// =============================================================================
// mark: Filters
// =============================================================================

  /**
   * Needed for pre v8 version
   */
  public function _wp_relative_upload_path( $new_path, $path ) {
    $private_dir = $this->get_basedir();

    if ( 0 === strpos( $new_path, $private_dir ) ) {
      $new_path = str_replace( $private_dir, '', $new_path );
      $new_path = ltrim( $new_path, '/' );
    }

    return $new_path;
  }

  public function add_post_metadata( $check, $object_id, $meta_key, $meta_value, $unique ) {
    if ( 'enclosure' === $meta_key ) {
      $args        = array(
        'post_parent'    => $object_id,
        'post_type'      => 'attachment',
        'posts_per_page' => -1,
        'orderby'        => 'menu_order',
        'order'          => 'ASC',
      );
      $attachments = get_children( $args );

      foreach ( $attachments as $attachment ) {
        $permissions = $this->get_permissions( $attachment->ID );

        if ( ! empty( $permissions ) && $this->pre_v8_protected( $attachment->ID ) ) {
          $site_url           = get_option( 'siteurl' );
          $private_upload_url = trailingslashit( $site_url ) . str_replace( ABSPATH, '', $this->get_basedir() );

          if ( false !== strpos( $meta_value, $private_upload_url ) ) {
            $check = false;
          }
        }
      }
    }

    return $check;
  }

  public function display_media_states( $media_states, $post ) {
    $permissions = $this->get_permissions( $post->ID );

    if ( ! empty( $permissions ) ) {
      if ( wp_doing_ajax() ) {
        $media_states[] = html_entity_decode('&#x1F6E1;') . __( 'Protected upload', 'dkzr-protected-uploads' ); // ajax media browser does not support HTML here so we use the unicode shield
      } else {
        $media_states[] = '<span class="dashicons dashicons-shield" tabindex="-1"></span>';
      }
    }

    return $media_states;
  }

  public function get_attached_file( $path, $attachment_id ) {
    $permissions = $this->get_permissions( $attachment_id );

    if ( ! empty( $permissions ) && $this->pre_v8_protected( $attachment_id ) ) {
      $upload_dir  = wp_get_upload_dir();
      $public_dir  = trailingslashit( $upload_dir['basedir'] );
      $private_dir = $this->get_pre_v8_basedir();
      $path        = str_replace( $public_dir, $private_dir, $path );
    }

    return $path;
  }

  public function wp_get_attachment_url( $url, $attachment_id ) {
    $permissions = $this->get_permissions( $attachment_id );

    if ( ! empty( $permissions ) && $this->pre_v8_protected( $attachment_id ) ) {
      $upload_dir         = wp_get_upload_dir();
      $site_url           = get_option( 'siteurl' );
      $public_upload_url  = trailingslashit( $upload_dir['baseurl'] );
      $private_upload_url = trailingslashit( $site_url ) . str_replace( ABSPATH, '', $this->get_pre_v8_basedir() );
      $url                = str_replace( $public_upload_url, $private_upload_url, $url );
    }

    return $url;
  }

  public function wp_calculate_image_srcset( $sources, $size_array, $image_src, $image_meta, $attachment_id ) {
    $permissions = $this->get_permissions( $attachment_id );

    if ( ! empty( $permissions ) && $this->pre_v8_protected( $attachment_id ) ) {
      $private_upload_url = trailingslashit( get_option( 'siteurl' ) ) . str_replace( ABSPATH, '', $this->get_pre_v8_basedir() );
      $upload_dir        = wp_get_upload_dir();
      $public_upload_url = trailingslashit( $upload_dir['baseurl'] );

      foreach ( $sources as $key => $url ) {
        $sources[ $key ] = str_replace( $public_upload_url, $private_upload_url, $url );
      }
    }

    return $sources;
  }

  public function wp_prepare_attachment_for_js( $response, $attachment, $meta ) {
    $response['protectedUpload'] = false;

    $permissions = $this->get_permissions( $attachment->ID );
    if ( ! empty( $permissions ) ) {
      $response['protectedUpload'] = $permissions;
    }
    return $response;
  }

  public function attachment_fields_to_edit( $form_fields, $attachment ) {
    $permissions = $this->get_permissions( $attachment->ID );

    $rules = $this->get_rules();

    $html = [];
    if ( isset( $rules['_deny_all'] ) && $rules['_deny_all'] ) {
      $html[] = sprintf(
        '<label><input type="checkbox" id="attachments-%1$d-dkzr_protected_uploads_permissions-_deny_all" name="attachments[%1$d][dkzr_protected_uploads_permissions][_deny_all]" value="1" %3$s>%2$s</label>',
        $attachment->ID,
        __( 'Deny all access (overrules all below)', 'dkzr-protected-uploads' ),
        checked( ( isset($permissions['_deny_all']) ? $permissions['_deny_all'] : null ), 1, 0 )
      );
      unset( $rules['_deny_all'] );
    }
    if ( isset( $rules['_require_login'] ) && $rules['_require_login'] ) {
      $html[] = sprintf(
        '<label><input type="checkbox" id="attachments-%1$d-dkzr_protected_uploads_permissions-_require_login" name="attachments[%1$d][dkzr_protected_uploads_permissions][_require_login]" value="1" %3$s>%2$s</label>',
        $attachment->ID,
        __( 'Require any login', 'dkzr-protected-uploads' ),
        checked( ( isset($permissions['_require_login']) ? $permissions['_require_login'] : null ), 1, 0 )
      );
      unset( $rules['_require_login'] );
    }
    foreach ( $rules as $rule => $label ) {
      $html[] = sprintf(
        '<label><input type="checkbox" id="attachments-%1$d-dkzr_protected_uploads_permissions-%2$s" name="attachments[%1$d][dkzr_protected_uploads_permissions][%2$s]" value="1" %4$s>%3$s</label>',
        $attachment->ID,
        esc_attr( $rule ),
        esc_html( $label ),
      checked( ( isset($permissions[$rule]) ? $permissions[$rule] : null ), 1, 0 )
      );
    }

    $form_fields['dkzr_protected_uploads'] = [
      'label' => esc_html__( 'Protected upload', 'dkzr-protected-uploads' ),
      'input' => 'html',
      'html'  => implode( '<br>', $html ),
      'helps' => esc_html__( '(Un)protecting an upload will change the URL of the file. You might need to re-insert media in your posts. Protected uploads are marked with the shield icon:', 'dkzr-protected-uploads' ) . '<span class="dashicons dashicons-shield" tabindex="-1"></span>',
    ];

    return $form_fields;
  }

  public function attachment_fields_to_save( $attachment, $attachment_data ) {
    $permissions = ( isset( $attachment_data['dkzr_protected_uploads_permissions'] ) && is_array( $attachment_data['dkzr_protected_uploads_permissions'] ) ? $attachment_data['dkzr_protected_uploads_permissions'] : [] );
    $permissions = apply_filters( 'dkzr_protected_uploads_attachment_permission_on_save', $permissions, $attachment );

    $rules = $this->get_rules();

    foreach( array_keys( $permissions ) as $role ) {
      if ( ! isset( $rules[$role] ) ) {
        unset( $permissions[$role] );
      }
    }

    if ( empty( $permissions ) ) {
      $this->move_media( $attachment['ID'], 'public', $attachment_data );
    } else {
      //if ( 0 === strpos( $attachment['post_mime_type'], 'video' ) ) {
      //  update_option( 'pvtmed_deactivate_migrate_video', true, false );
      //}

      //if ( 0 === strpos( $attachment['post_mime_type'], 'audio' ) ) {
      //  update_option( 'pvtmed_deactivate_migrate_audio', true, false );
      //}

      $this->move_media( $attachment['ID'], 'private', $attachment_data );
    }

    update_post_meta( $attachment['ID'], 'dkzr_protected_uploads_permissions', $permissions );

    return $attachment;
  }

// =============================================================================
// mark: Non-WordPress actions and filters
// =============================================================================

  public function ai1wmme_export_enumerate_media( $params ) {
    require_once __DIR__ . '/includes/ai1wmme-export-enumerate-protected-media.php';

    return Ai1wmme_Export_Enumerate_Protected_Media::execute( $params );
  }

  public function ai1wm_export_enumerate_media( $params ) {
    require_once __DIR__ . '/includes/ai1wm-export-enumerate-protected-media.php';

    return Ai1wm_Export_Enumerate_Protected_Media::execute( $params );
  }

  public function ai1wm_export_media( $params ) {
    require_once __DIR__ . '/includes/ai1wm-export-protected-media.php';

    return Ai1wm_Export_Protected_Media::execute( $params );
  }

/**
 * Enable Media Replace v3.3+: supported!
 */
  public function emr_wp_handle_replace( $post ) {
    if ( isset( $post['post_id'] ) ) {
      $permissions = $this->get_permissions( $post['post_id'] );
      if ( ! empty( $permissions ) && isset( $form_fields['enable-media-replace']['html'] ) ) {
        $this->delete_attachment( $post['post_id'] );
      }
    }
  }

/**
 * Pre Enable Media Replace v3.3: unsupported
 */
  public function emr_attachment_fields_to_edit( $form_fields, $attachment ) {
    $permissions = $this->get_permissions( $attachment->ID );
    if ( ! empty( $permissions ) && isset( $form_fields['enable-media-replace']['html'] ) ) {
      $form_fields['enable-media-replace']['html'] = "<p><a class='button-secondary disabled' href='javascript:;'>" . esc_html__("Upload a new file", "enable-media-replace") . "</a></p>";
      $form_fields['enable-media-replace']['helps'] = __( 'Enable Media Replace currently is not supported for Protected Uploads.', 'dkzr-protected-uploads' );
    }


    return $form_fields;
  }

/**
 * Pre Enable Media Replace v3.3: unsupported
 */
  public function emr_before_replace_type_options() {
    $permissions = $this->get_permissions( (int) $_GET["attachment_id"] );

    if ( ! empty( $permissions ) ) {
      printf( '<div id="message" class="error"><p>%s</p></div>', esc_html__( 'Enable Media Replace currently is not supported for Protected Uploads.', 'dkzr-protected-uploads' ) );

      add_filter( 'emr_display_replace_type_options', '__return_false', 9999 );
      add_filter( 'emr_enable_replace_and_search', '__return_false', 9999 );
    }
  }
}

function dkzr_protect_upload( $attachment, $permissions = [], $overwrite = false ) {
  global $dkzrProtectedUploads;

  $attachment = get_post( $attachment, ARRAY_A );

  if ( ! $attachment ) {
    return;
  }

  if ( ! $overwrite ) {
    $permissions = array_merge( $dkzrProtectedUploads->get_permissions( $attachment['ID'] ), $permissions );
  }

  $dkzrProtectedUploads->attachment_fields_to_save( $attachment, [ 'dkzr_protected_uploads_permissions' => $permissions ] );
}

function dkzr_protect_upload_allowed( $attachment ) {
  global $dkzrProtectedUploads;

  $attachment = get_post( $attachment, ARRAY_A );

  if ( ! $attachment ) {
    return false;
  }

  return $dkzrProtectedUploads->is_allowed( $attachment['ID'], true );
}

global $dkzrProtectedUploads;
$dkzrProtectedUploads = new dkzrProtectedUploads();
