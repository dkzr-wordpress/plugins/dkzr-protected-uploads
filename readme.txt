=== Protected Uploads ===
Contributors: joostdekeijzer
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=j@dkzr.nl&item_name=dkzr-protected-uploads+WordPress+plugin&item_number=Joost+de+Keijzer&currency_code=EUR
Tags: media, image, photo, picture, mp3, video, protection
Requires at least: 5.0
Tested up to: 5.3
Stable tag: 4.2.2
Requires PHP: 7.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add access restrictions to specific items of the WordPress Media Library.

== Description ==

== Installation ==

* Download the plugin
* Uncompress it with your preferred unzip program
* Copy the entire directory in your plugin directory of your WordPress blog (/wp-content/plugins)
* Activate the plugin
* Go to an item in your media library
* See the "Protected Upload" checkboxes!

== Frequently Asked Questions ==

= Where are the protected media stored? =

By default, the plugin tries to create the folder 'protected-uploads' next to the default 'uploads' folder that WordPress makes for you.

With default settings, this should all work fine. When you've set custom values to eg. ABSPATH, WP_CONTENT_DIR, UPLOADS, BLOGUPLOADDIR, UPLOADBLOGSDIR etc. you should check if all is how you want it to be.

= Is IIS supported? =

Nope, not for now. I can't test the web.config that I need to generate.

== Developers ==

The following filters and actions are available:

= `dkzr_protected_uploads_datadir_name` filter =

Input: (string) `$datadir_name`: data directory name, defaults to 'protected-uploads'.

Output: (string) `$datadir_name`

= `dkzr_protected_uploads_basedir` filter =

Input:

1. (path) `$basedir`: absolute path to upload dir (where datadir_name is already appended), defaults to calculated value derived from WordPress `wp_get_upload_dir()`.
1. (interger) `$blog_id`: current blog ID

Output: (string) `$basedir`

= `dkzr_protected_uploads_roles` filter =

Input: (array) `$roles`: list of roles that can be given access to an attachment. Defaults to two special roles (see below) and all current WP Roles names.

Output: (array) `$roles`

There are two special keys in the roles list:

* `_deny_all`: deny all access for any user
* `_require_login`: only require a valid login, no special WordPress role needed.

= `dkzr_protected_uploads_attachment_permission` filter =

Input:

1. (bool) `$allow`: is access allowed to this attachment?
1. (url) `$file_url`: the requested file url
1. (array) `$permissions`: permissions settings for this attachment
1. (integer) `$attachment_id`: current attachment ID

Output: (bool) `$allow` (true = allowed, false = denied)

= `dkzr_protected_uploads_headers` action =

Input:

1. (url) `$file_url`: the requested file url
1. (integer) `$attachment_id`: current attachment ID
1. (object) `$wp`: the WordPress class instance containing eg. `$wp->query_vars` and `$wp->request`

== Changelog ==

= 4.2 =

* bugfix: changed parameter order in `dkzr_protected_uploads_attachment_permission` reset, new parameter added to the end.

= 4.1 =

* added `dkzr_protected_uploads_basedir` filter
* added `dkzr_protected_uploads_headers` action
* added developer section to readme
* changed `dkzr_protected_uploads_attachment_permission` filter parameters

= 4.0 =

* Changed post_meta 'dkzr_protected_uploads_is_private' to integer value 0 or 1
* bugfix attachemnt_metadata as an argument

= 3.3 =

* prevent potential `readfile` memory issues
* allow attachment metadata as argument

= 3.2.2 =

* better frontend find attachment code for generated images
* PDF generated preview support

= 3.2.1 =

* emr fix

= 3.2 =

* lazy-load WP_Filesystem
* More "enable media replace" support (v3.3+)

= 3.1 =

* delete attachments (and generated files) improvements
* "enable media replace" plugin support

= 3.0 =

* basedir handling fixes & improvements

= 2.0.2 =

* No IIS support (for now), I can't test the web.config settings...

= 2.0.1 =

* old-style multisite fix

= 2.0 =

* use parse_request action for download logic

= 1.1.2 =

* add status header 200

= 1.1.1 =

* javascript bugfix

= 1.1 =

* improved `move` method
* update url in views
* display shield icon in media browser
* better permission error

= 1.0 =

* Multisite support

= 0.9 =

* Initial version

== Upgrade Notice ==
